package ru.tsc.fuksina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.service.IAuthService;
import ru.tsc.fuksina.tm.api.service.IUserService;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.exception.field.LoginEmptyException;
import ru.tsc.fuksina.tm.exception.field.PasswordEmptyException;
import ru.tsc.fuksina.tm.exception.field.PasswordIncorrectException;
import ru.tsc.fuksina.tm.exception.system.AccessDeniedException;
import ru.tsc.fuksina.tm.exception.system.AuthenticationException;
import ru.tsc.fuksina.tm.exception.system.PermissionException;
import ru.tsc.fuksina.tm.model.User;
import ru.tsc.fuksina.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        @NotNull final User user = Optional.ofNullable(userService.findOneByLogin(login))
                .orElseThrow(AuthenticationException::new);
        if (user.getLocked()) throw new PermissionException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new PasswordIncorrectException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        @NotNull final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRoles(Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
