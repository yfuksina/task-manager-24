package ru.tsc.fuksina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description);

}
